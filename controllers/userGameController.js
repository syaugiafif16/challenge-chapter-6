const { user_game, user_game_biodata,user_game_histories} = require('../models');


module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game.findAll({
                include :[
                    {model:user_game_biodata},
                    {model:user_game_histories}

                ]
            });
           

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error);
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game.create({
                username: req.body.username,
                password: req.body.password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user_game.update({
                username: req.body.username,
                password: req.body.password
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
