const {
    user_game_histories,
    user_game
} = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game_histories.findAll({
                    include: [{
                        model: user_game,
                        as: 'user_game'
                    }]
                }

            );
            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error);
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_histories.create({
                games_played: req.body.games_played,
                games_won: req.body.games_won,
                games_lose: req.body.games_lose,
                user_game_id : req.body.user_game_id 

            });

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error);
            return res.json({

                message: "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game_histories.update({
                games_played: req.body.games_played,
                games_won: req.body.games_won,
                games_lose: req.body.games_lose
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_histories.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    }
}