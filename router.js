const express = require('express')
const router = express.Router()
const userGameController = require('./controllers/userGameController');
const userGameBiodataController = require('./controllers/userGameBiodataController');
const userGameHistoryController = require('./controllers/userGameHistoryController');


router.get('/user_game/list', userGameController.list)
router.post('/user_game/create', userGameController.create)
router.put('/user_game/update', userGameController.update)
router.delete('/user_game/destroy', userGameController.destroy)

router.get('/user_game_biodata/list', userGameBiodataController.list)
router.post('/user_game_biodata/create', userGameBiodataController.create)
router.put('/user_game_biodata/update', userGameBiodataController.update)
router.delete('/user_game_biodata/destroy', userGameBiodataController.destroy)

router.get('/user_game_history/list', userGameHistoryController.list)
router.post('/user_game_history/create', userGameHistoryController.create)
router.put('/user_game_history/update', userGameHistoryController.update)
router.delete('/user_game_history/destroy', userGameHistoryController.destroy)



module.exports = router
